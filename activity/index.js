/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/ 
	//first function here:
	function details(){
		let fullName = prompt('enter full name');
		let age = prompt('enter age');
		let location = prompt('enter location');

		console.log('Hi ' + fullName + '!');
		console.log(fullName + "'s age is " + age );
		console.log(fullName + '\'s ' + 'location is at ' + location);

	}
	details();
/*	
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function faveBand(){
		let bands = ['Metallica', 'Megadeth', 'Iron Maiden', 'Judas Priest', 'Log'];
		console.log(bands);
	}
	faveBand();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies(){
		let mov1 = '1. Inception';
		let mov2 = '2. The Dark Knight';
		let mov3 = '3. Interstellar';
		let mov4 = '4. 1917';
		let mov5 = '5. No Time to Die';

		console.log(mov1);
		console.log('Tomato meter for ' + mov1 + ' 87%')
		console.log(mov2);
		console.log('Tomato meter for ' + mov2 + ' 94%')
		console.log(mov3);
		console.log('Tomato meter for ' + mov3 + ' 73%')
		console.log(mov4);
		console.log('Tomato meter for ' + mov4 + ' 88%')
		console.log(mov5);
		console.log('Tomato meter for ' + mov5 + ' 83%')
	}
	faveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);