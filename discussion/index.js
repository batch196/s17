console.log('hello world');

//functions
/*//function functionName() {
	code block(statemant)
}*/

function printName() {
	console.log('My name is Anya');
};

printName();
//result My name is anya


// declaredFunction(); not yet declared

//function declaration vs expression

declaredFunction();
function declaredFunction() {
	console.log('I am from declaredFunction');
};
declaredFunction();

//function expression

	//Anonymous function - function without a name

let variableFunction = function() {
	console.log('i am from variableFunction');
};
variableFunction();

let funcExpression = function funcName(){
	console.log('hello fron the other side');
};
funcExpression();

// you can reassign declared function and function expression to new anonymous function

declaredFunction = function(){
	console.log('updated declaredFunction');
}
declaredFunction();

funcExpression = function(){
	console.log('updated funcExpression');
};

const constantFunction = function(){
	console.log('initialized with cont');
};

constantFunction();

/*constantFunction = function(){
	console.log('cannot be reassigned');
};
constantFunction();
//reassignment with const expression is not possible

*/
//function scoping 
/*
	
	javascript variables has 3 types of scope
	1. local / block scope
	2. global scope
	3. function scope

*/
{
let localVar = 'Armando Perez';
}

let globalVar = 'Mr. worldWide';

console.log(globalVar);
// console.log(localVar);

function showNames(){
	//function scope variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'kevin';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};
showNames();

// nested functions

function myNewFunction() {
	let name = 'yor';

	function nestedFunction(){
		let nestedName = 'brando';
		console.log(nestedName);
	}
	nestedFunction();
}
myNewFunction();

//global scope variable

let globaName = 'alan';

function myNewFunction2(){
	let nameInsidec = 'marco';
	console.log(globaName);

};
myNewFunction2();
//result alan

// //alert
// alert('hello world');

// function showSampleAlert(){
// 	alert('hello');
// };

// showSampleAlert();
console.log('i will inly login the console when the alert is dismissed')

//prompt()
//syntax prompt("<dialog>")

/*let samplePrompt = prompt('enter your name');
console.log('hello ' + samplePrompt);

let sampleNullPrompt = prompt('dont enter anything');
console.log(samplePrompt);*/

function printWelcomeMessage(){
	let firstName = prompt('enter first name');
	let lastName = prompt('enter last name');

	console.log('hello ' + firstName + '' + lastName + '!');
	console.log('welcome to my page');
}
printWelcomeMessage();

// function naming conventions

function getCourses(){
	let courses = ['science', 'math', 'english'];
	console.log(courses); 
}
getCourses();

function get(){
	let name = 'anya';
	console.log(name);
}
get();

//avoid pointless and inappropriate function names

function foo(){
	console.log(25 % 5);
}
foo();

//name your function in small caps follow camelcase

function displayCarInfo(){
	console.log('Barand: Toyota');
	console.log('type: sedan');
	console.log('price: 1,500,00');
}
displayCarInfo();

